<?php
/**
 * Интернет-программирование. Задача 8.
 * Реализовать скрипт на веб-сервере на PHP или другом языке,
 * сохраняющий в XML-файл заполненную форму задания 7. При
 * отправке формы на сервере создается новый файл с уникальным именем.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Thank you, date is  save!';
    }
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['error_fio']);
    $errors['mail'] = !empty($_COOKIE['error_mail']);
    $errors['date1'] = !empty($_COOKIE['error_date1']);
    $errors['r_g_1'] = !empty($_COOKIE['error_r_g_1']);
    $errors['r_g_2'] = !empty($_COOKIE['error_r_g_2']);
    $errors['biogr'] = !empty($_COOKIE['error_biogr']);
    $errors['agree'] = !empty($_COOKIE['error_agree']);
    $errors['herous']=!empty($_COOKIE['error_herous']);
    
    if ($errors['fio']){
        setcookie('error_fio', '', 1000);
        $messages[] = '<div class = "error">put name</div>';
    }
    if ($errors['mail']){
        if ($_COOKIE['error_mail'] == '1'){
            setcookie('error_mail', '', 1000);
            $messages[] = '<div class = "error">put mail</div>';
        }
        else {
          setcookie('error_mail', '', 1000);
        $messages[] = '<div class = "error">put right mail</div>';  
        }
        
    }
    if ($errors['date1']){
        setcookie('error_date1', '', 1000);
        $messages[] = '<div class = "error">put date1</div>';
    }
    if ($errors['r_g_1']){
        setcookie('error_r_g_1', '', 1000);
        $messages[] = '<div class = "error">put gender</div>';
    }
    if ($errors['r_g_2']){
        setcookie('error_r_g_2', '', 1000);
        $messages[] = '<div class = "error">put legs</div>';
    }
    if ($errors['biogr']){
        setcookie('error_biogr', '', 1000);
        $messages[] = '<div class = "error">put biography</div>';
    }
    if ($errors['agree']){
        setcookie('error_agree', '', 1000);
        $messages[] = '<div class = "error">You must agree</div>';
    }
    if ($errors['herous']){
        setcookie('error_herous', '',1000);
        setcookie('idclip', '', 1000);
        setcookie('fly', '', 1000);
        setcookie('deathless', '', 1000);
        $messages[] = '<div class = "error">choose superpower</div>';
    }
    
    
    // Включаем содержимое файла form.php.
    include('form.php');
    // Завершаем работу скрипта.
    exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['fio'])) {
    setcookie('error_fio', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('fio', $_POST['fio'], time()+30*24*60*60);
}
if (empty($_POST['mail'])) {
    setcookie('error_mail', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('mail', $_POST['mail'], time()+30*24*60*60);
}
if (empty($_POST['date1'])) {
    setcookie('error_date1', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    if(!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|
([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $_POST['mail'])){
setcookie('error_mail', '2', time()+24*60*60);
$errors = TRUE;
    }
    setcookie('date1', $_POST['date1'], time()+30*24*60*60);
}
if (empty($_POST['radio_group_1'])) {
    setcookie('error_r_g_1', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('r_g_1', $_POST['radio_group_1'], time()+30*24*60*60);
}
if (empty($_POST['radio-group_2'])) {
    setcookie('error_r_g_2', '1', time()+24*60*60);;
    $errors = TRUE;
}
else{
    setcookie('r_g_2', $_POST['radio-group_2'], time()+30*24*60*60);
}
if (empty($_POST['biography'])){
    setcookie('error_biogr', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('biogr', $_POST['biography'], time()+30*24*60*60);
}
if ($_POST['agree']==='0'){
    setcookie('error_agree', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('agree', $_POST['agree'], time()+30*24*60*60);
}
if ($_POST['herous'] === 'fly'){
    $fly = '1';
}
else $fly = '0';
if ($_POST['herous'] === 'idclip'){
    $idclip = "1";
} else $idclip = '0';
if ($_POST['herous'] === 'deathless'){
    $deathless = "1";
}else $deathless = '0';
if ($fly!="1" && $idclip != "1" && $deathless != "1"){
    setcookie('error_herous', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('idclip', $idclip, time()+30*24*60*60);
    setcookie('deathless', $deathless, time()+30*24*60*60);
    setcookie('fly', $fly, time()+30*24*60*60);
}
// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
    header('Location: index.php');
    // При наличии ошибок завершаем работу скрипта.
    exit();
}

// Сохранение в базу данных.

$user = 'u20341';
$pass = '3227715';
$db = new PDO('mysql:host=localhost;dbname=u20341', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
    $fio = $_POST['fio'];
    $mail = $_POST['mail'];
    $birthday = $_POST['date1'];
    if ($_POST['radio_group_1'] === 'man'){
        $gender = 'man';
    }
    else{
        $gender = 'woman';
    }
    if ($_POST['radio-group_2']==='1'){
        $legs = 1;
    } else if ($_POST['radio-group_2']==='2'){
        $legs = 2;
    } else if ($_POST['radio-group_2']==='3'){
        $legs = 3;
    }
     
    
    
    
    if ($_POST['agree'] === '1'){
        $a = '1';
    } else $a=0;
    $stmt = $db->prepare("INSERT INTO task_3 SET name = ?, email = ?, birthday = ?, gender = ?, legs = ?, 
deathless = ?, idclip = ?, fly = ?, biography = ?, agreement = ?");
    $stmt -> execute(array($fio, $mail, $birthday, $gender, $legs, $deathless, $idclip, $fly,
        $_POST['biography'], $a ) );
   
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}

//  stmt - это "дескриптор состояния".

//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));

//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
 $stmt->bindParam(':firstname', $firstname);
 $stmt->bindParam(':lastname', $lastname);
 $stmt->bindParam(':email', $email);
 $firstname = "John";
 $lastname = "Smith";
 $email = "john@test.com";
 $stmt->execute();
 */

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
setcookie('save', '1');
header('Location: index.php');

